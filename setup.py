#!/usr/bin/python3
# -*- coding: UTF-8 -*-

from setuptools import setup
from setuptools import find_packages


setup(
    name='xivo-bus',
    version='0.1',
    description='XiVO BUS libraries',
    author='Avencall',
    author_email='dev@avencall.com',
    url='http://git.xivo.io/',
    license='GPLv3',
    packages=find_packages(),
)
