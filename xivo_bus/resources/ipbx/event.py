# -*- coding: utf-8 -*-

# Copyright (C) 2018 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


class ReloadIpbxEvent(object):
    name = 'ipbx_reload'
    routing_key = 'config.ipbx.reload'

    def __init__(self, commands_list):
        self.commands = commands_list

    def marshal(self):
        return {
            'commands': self.commands
        }

    @classmethod
    def unmarshal(cls, msg):
        return cls(msg['commands'])

    def __eq__(self, other):
        return self.commands == other.commands

    def __ne__(self, other):
        return self.commands != other.commands
