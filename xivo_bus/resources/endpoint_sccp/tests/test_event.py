# -*- coding: utf-8 -*-

# Copyright (C) 2015 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import unittest

from ..event import SccpEndpointConfigEvent

ID = 42


class ConcreteSccpEndpointConfigEvent(SccpEndpointConfigEvent):
    name = 'foo'


class TestAbstractSccpEndpointIDParams(unittest.TestCase):

    def setUp(self):
        self.msg = {
            'id': ID,
        }

    def test_marshal(self):
        command = ConcreteSccpEndpointConfigEvent(ID)

        msg = command.marshal()

        self.assertEqual(msg, self.msg)

    def test_unmarshal(self):
        command = ConcreteSccpEndpointConfigEvent.unmarshal(self.msg)

        self.assertEqual(command.name, ConcreteSccpEndpointConfigEvent.name)
        self.assertEqual(command.id, ID)
