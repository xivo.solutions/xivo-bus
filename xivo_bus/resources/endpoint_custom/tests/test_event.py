# -*- coding: utf-8 -*-

# Copyright (C) 2016 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


import unittest

from ..event import CustomEndpointConfigEvent

ID = 42
INTERFACE = 'dahdi/i1'


class ConcreteCustomEndpointConfigEvent(CustomEndpointConfigEvent):
    name = 'foo'


class TestAbstractCustomEndpointIDParams(unittest.TestCase):

    def setUp(self):
        self.msg = {
            'id': ID,
            'interface': INTERFACE,
        }

    def test_marshal(self):
        command = ConcreteCustomEndpointConfigEvent(ID, INTERFACE)

        msg = command.marshal()

        self.assertEqual(msg, self.msg)

    def test_unmarshal(self):
        command = ConcreteCustomEndpointConfigEvent.unmarshal(self.msg)

        self.assertEqual(command.name, ConcreteCustomEndpointConfigEvent.name)
        self.assertEqual(command.id, ID)
        self.assertEqual(command.interface, INTERFACE)
