# -*- coding: utf-8 -*-

# Copyright (C) 2020 Avencall
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


from xivo_bus.resources.common.event import ResourceConfigEvent


class EditLabelEvent(ResourceConfigEvent):
    name = 'label_edited'
    routing_key = 'config.label.edited'


class CreateLabelEvent(ResourceConfigEvent):
    name = 'label_created'
    routing_key = 'config.label.created'


class DeleteLabelEvent(ResourceConfigEvent):
    name = 'label_deleted'
    routing_key = 'config.label.deleted'
